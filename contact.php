<!DOCTYPE html>
<html lang="vn">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="assets/css/styles.css">

        <title>Web ERB</title>
</head>
<body>


<a href="#" class="scrolltop" id="scroll-top">
            <i class='bx bx-chevron-up scrolltop__icon'></i>
        </a>

        <header class="l-header" id="header">
            <nav class="nav bd-container">
                <a href="index.php" class="nav__logo">ERP</a>

                <div class="nav__menu" id="nav-menu">
                    <ul class="nav__list">
                        <li class="nav__item"><a href="index.php" class="nav__link active-link">Trang Chủ</a></li>
                        <li class="nav__item"><a href="profit.php" class="nav__link">Lợi Ích</a></li>
						<li class="nav__item"><a href="feature.php" class="nav__link">Tính Năng</a></li>
                        <li class="nav__item"><a href="services.php" class="nav__link">Dịch Vụ</a></li>
						<li class="nav__item"><a href="guide.php" class="nav__link">Hướng Dẫn</a></li>
                        <li class="nav__item"><a href="member.php" class="nav__link">Đăng Nhập</a></li>
                        <li class="nav__item"><a href="contact.php" class="nav__link">Liên Hệ</a></li>

                        <li><i class='bx bx-moon change-theme' id="theme-button"></i></li>
                    </ul>
                </div>

                <div class="nav__toggle" id="nav-toggle">
                    <i class='bx bx-menu'></i>
                </div>
            </nav>
        </header>

<div class="container" style="margin-top:30px">
  <div class="row">
     
  </div>
</div>

<!--FOOTER-->
        <footer class="footer section bd-container">
            <div class="footer__container bd-grid">
                <div class="footer__content">
                    <a href="#" class="footer__logo">ERP</a>
                    <span class="footer__description">ỨNG DỤNG QUẢN LÝ CÔNG VIỆC</span>
                    <div>
                        <a href="#" class="footer__social"><i class='bx bxl-facebook'></i></a>
                        <a href="#" class="footer__social"><i class='bx bxl-instagram'></i></a>
                        <a href="#" class="footer__social"><i class='bx bxl-twitter'></i></a>
                    </div>
                </div>

                <div class="footer__content">
                    <h3 class="footer__title">Dịch vụ</h3>
                    <ul>
                        <li><a href="#" class="footer__link">Kế hoạch</a></li>
                        <li><a href="#" class="footer__link">Tư vấn</a></li>
                        <li><a href="#" class="footer__link">Chiến lượt</a></li>
                        <li><a href="#" class="footer__link">Kiến thức</a></li>
                    </ul>
                </div>

                <div class="footer__content">
                    <h3 class="footer__title">Thông tin</h3>
                    <ul>
                        <li><a href="#" class="footer__link">Sự kiện</a></li>
                        <li><a href="contact.php" class="footer__link">Liên hệ</a></li>
                        <li><a href="#" class="footer__link">Chính sách</a></li>
                        <li><a href="#" class="footer__link">Góp ý</a></li>
                        <li><a href="member.php" class="footer__link">Đăng nhập</a></li>
                    </ul>
                </div>

                <div class="footer__content">
                    <h3 class="footer__title">Địa chỉ</h3>
                    <ul>
                        <li>Hồ Chí Minh - VietNam</li>
                        <li>123 - 456 - 789</li>
                        <li>ERP@gmail.com</li>
                    </ul>
                </div>
            </div>

            <p class="footer__copy">&#169; ERP</p>
        </footer>

        <!--SCROLL REVEAL-->
        <script src="https://unpkg.com/scrollreveal"></script>

        <!--MAIN JS-->
        <script src="assets/js/main.js"></script>

</body>
</html>