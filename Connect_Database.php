
<?php

class ketnoi {

    public $con, $rs;

    //viet cac phuong thuc getset
    //xay dung phuong thuc mo ket noi
    function ketnoi() {
        $this->con = mysqli_connect("localhost", "root", "");
        if (!$this->con) {
            die("loi ket noi " . mysqli_connect_errno() . "- " . mysqli_connect_error());
        } else {
            if (!mysqli_select_db($this->con, 'minhnguyenn')) {
                die("loi co so du lieu " . mysqli_connect_errno() . "- " . mysqli_connect_error());
            } else {

                //$this->con->set_charset("utf8");
                mysqli_set_charset($this->con, "utf8");
            }
        }
    }

    //xây dung phuong thuc truy van du lieu	
    function truyvan_sql($sql) {
        //thực thi câu truy vấn
        $this->rs = mysqli_query($this->con, $sql);
        if (!$this->rs)
            die("loi co truy van du lieu r " . mysqli_connect_errno() . "- " . mysqli_connect_error());
        else
            return $this->rs;
    }

    //load tat ca cac dong
    public function LoadAllRow($sql) {
        if (!mysqli_query($this->con, $sql)) {
            die("loi co truy van du lieu " . mysqli_connect_errno() . "- " . mysqli_connect_error());
        } else {
            $this->rs = mysqli_query($this->con, $sql);
            $array = array();
            while ($row = mysqli_fetch_array($this->rs)) {
                $array[] = $row;
            }
            mysqli_free_result($this->rs);
            return $array;
        }
    }

    //phuong thu dong ket net noi
    function dong_kn() {
        mysqli_free_result($rs);
        mysqli_close($this->con);
    }

}

?>
